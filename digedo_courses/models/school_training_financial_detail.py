from odoo import _, models, fields, api


class SchoolTrainingFinancialDetail(models.Model):
    _name = 'school.training.financial.detail'
    _description = 'Training Financial Detail'

    training_id = fields.Many2one('school.training', 'Training')
    item_id = fields.Many2one('budget.item', required=True)
    student_id = fields.Many2one('res.partner')
    cur_number = fields.Char(store=True)
    amount = fields.Float(digits=(9,2), required=True)
    commentaries = fields.Text(store=True)
    file = fields.Binary(attachment=True)
    file_name = fields.Char()

    @api.model
    def create(self, values):
        b = values.get('commentaries')
        if b:
            values.update({'commentaries': b.upper()})
        return super(SchoolTrainingFinancialDetail, self).create(values)

    @api.multi
    def write(self, values):
        b = values.get('commentaries')
        if b:
            values.update({'commentaries': b.upper()})
        return super(SchoolTrainingFinancialDetail, self).write(values)

    @api.multi
    def name_get(self):
        if not self.ids:
            return []
        res = []
        for record in self:
            res.append((record.id, record.item_id))
        return res