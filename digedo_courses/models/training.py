# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class TypeOfTraining(models.Model):
    _name = 'courses.training.type'
    _description = 'Type of Training'

    name = fields.Char(string='DESCRIPCION', size=128, required=True)
    valuation_ids = fields.One2many('courses.training.valuation', 'type_id', string='Items', readonly=True)

    @api.model
    def create(self, values):
        a = values.get('name')
        values.update({'name': a.upper()})
        return super(TypeOfTraining, self).create(values)

    @api.multi
    def write(self, values):
        a = values.get('name')
        if a:
            values.update({'name': a.upper()})
        return super(TypeOfTraining, self).write(values)

    @api.one
    @api.returns('self', lambda value: value.id)
    def copy(self, default=None):
        default = dict(default or {}, name=_('%s (Copy)') % self.name)
        return super(TypeOfTraining, self).copy(default=default)


class Valuation(models.Model):
    _name = 'courses.training.valuation'
    _description = 'Valuation'

    name = fields.Char(string='DESCRIPCION', size=128, required=True)
    score = fields.Float(string='PUNTAJE', digits=(1, 3), required=True, default=0)
    note = fields.Text(string='OBSERVACION')
    type_id = fields.Many2one('courses.training.type', string='TIPO DE CURSO', ondelete='cascade', required=True)

    _sql_constraints = [
        ('score_chk', 'check(score > 0)', _('The score must be grater than 0')),
    ]

    @api.multi
    @api.depends('name', 'score')
    def name_get(self):
        res = []
        for record in self:
            name = '%s (%.3f)' % (record.name, record.score)
            res.append((record.id, name))
        return res

    @api.model
    def create(self, values):
        a = values.get('name')
        values.update({'name': a.upper()})
        return super(Valuation, self).create(values)

    @api.multi
    def write(self, values):
        a = values.get('name')
        if a:
            values.update({'name': a.upper()})
        return super(Valuation, self).write(values)

    @api.one
    @api.returns('self', lambda value: value.id)
    def copy(self, default=None):
        default = dict(default or {}, name=_('%s (Copy)') % self.name)
        return super(Valuation, self).copy(default=default)
