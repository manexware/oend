from odoo import _, models, fields


class SieReligion(models.Model):
    _name = 'sie.religion'

    name = fields.Char(string='Name', required=True)
    code = fields.Char('Code', size=4, required=True)

    _sql_constraints = [
        ('name_uk', 'unique(name)', _('Name must be unique'))
    ]
