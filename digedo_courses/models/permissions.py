# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
import locale
from datetime import datetime
from odoo.exceptions import UserError
from dateutil.relativedelta import relativedelta

TIME_PERIOD = [
    ('trimestre', 'TRIMESTRE'),
    ('semestre', 'SEMESTRE'),
    ('anual', 'ANUAL'),
    ('bimensual','BIMENSUAL')
]

STATE = [
    ('draft', 'BORRADOR'),
    ('accepted', 'ACEPTADO'),
    ('done', 'FINALIZADO')
]

STATUS_FORM = [
    ('done', 'ESTUDIOS FINALIZADOS'),
    ('able', 'HABILITADO PARA RENOVACIÓN'),
    ('disable', 'NO HABILITADO PARA RENOVACIÓN')
]
TYPE = [
    ('new','NUEVA SOLICITUD'),
    ('renewal','RENOVACIÓN')
]


class Permissions(models.Model):
    _name = 'courses.training.permission'
    _description = 'Permissions'

    sequential_id = fields.Char(string='OFICIO NO.', store=True, readonly=True)
    #form_id = fields.Char(string='REFERENCIA', required=True,
     #                     states={'draft': [('readonly', False)]}, readonly=True)
    name = fields.Many2one('course.name', size=96, required=True,
                           readonly=True, states={'not executed': [('readonly', False)]})
    type = fields.Selection(TYPE, store=True, required=True, default='new',
                            states={'draft': [('readonly', False)]}, readonly=True)
    form_date = fields.Date(string='FECHA', required=True, default=fields.Date.context_today,
                            states={'draft': [('readonly', False)]}, readonly=True)
    hidden_date = fields.Char(compute='_compute_get_date', store=True)
    category_id = fields.Many2one('courses.level', string='NIVEL DE ESTUDIOS', ondelete='restrict',
                                  states={'draft': [('readonly', False)]}, readonly=True, required=True)
    time_period = fields.Selection(TIME_PERIOD, string="PERIODO DE TIEMPO", required="True", default="trimestre",
                                   states={'draft': [('readonly', False)]}, readonly=True)
    period_number = fields.Integer(string="NÚMERO DE PERIODO", states={'draft': [('readonly', False)]},
                                   readonly=True, required=True)
    start_date = fields.Date(string='FECHA DE INICIO', help='(mm/dd/yyyy)', required=True,
                             readonly=True, states={'draft': [('readonly', False)]}, default=fields.Date.context_today)
    end_date = fields.Date(string=u'FECHA DE FINALIZACIÓN', help='(mm/dd/yyyy)', required=True,
                           readonly=True, states={'draft': [('readonly', False)]}, default=fields.Date.context_today)
    duration = fields.Char(compute='_compute_duration', string=u'DURACIÓN', required=True,
                           states={'draft': [('readonly', False)]})

    applicant_id = fields.Many2one('res.partner', string='ALUMNO', ondelete='restrict', required=True,
                                   states={'draft': [('readonly', False)]}, readonly=True)
    citizen_id = fields.Char(related='applicant_id.citizen_id', readonly=True)
    responsability_id = fields.Char(related='applicant_id.responsability', readonly=True)
    division_id = fields.Many2one(related='applicant_id.division_id', readonly=True)
    # service_time = fields.Char(related='applicant_id.year', readonly=True)
    school_id = fields.Many2one('courses.school', string=u'INSTITUCIÓN ACADEMICA', ondelete='restrict', required=True,
                                states={'draft': [('readonly', False)]}, readonly=True)
    career_id = fields.Many2one('career.school', ondelete='restrict',
                                states={'draft': [('readonly', False)]}, readonly=True)
    state = fields.Selection(STATE, 'ESTADO', default='draft')
    schedule_ids = fields.One2many('schedule.permission', inverse_name="school_permission_id", string="HORARIOS",
                                   states={'draft': [('readonly', False)]}, readonly=True, required=True)
    file_ids = fields.One2many('permission.file', inverse_name="permission_id", string='ARCHIVOS ADJUNTOS')
    status_form = fields.Selection(STATUS_FORM, readonly=True)
    subject_curriculum_ids = fields.One2many('subject.curriculum', inverse_name="curriculum_id")
    sender = fields.Char(u'DESTINATARIO', store=True)
    #DIRECTIVA
    director_id = fields.Many2one('principal.table', string=u'DIRECTIVA', store=True)
    director = fields.Many2one(related="director_id.name", readonly=True, store=True)
    grade = fields.Many2one(related="director_id.grade", readonly=True, store=True)
    signature = fields.Char(u'SUMILLA', related="director_id.signature", readonly=True, store=True)
    institution = fields.Char(u'ESCUELA / DIRECCIÓN', related="director_id.institution", readonly=True, store=True)

    # @api.multi
    # @api.depends('applicant_id')
    # def _compute_total_score(self):
    #     obj = self.env['courses.training.permission']
    #     for record in self:
    #         x = 0
    #         if record.applicant_id:
    #             result = obj.search_read([('applicant_id', '=', record.applicant_id.id)], fields=['score'])
    #             x = sum(x['score'] for x in result)
    #         record.total_score = x

    # @api.onchange('type_id')
    # def onchange_type_id(self):
    #     if self.valuation_id:
    #         self.valuation_id = False

    # @api.one
    # @api.returns('self', lambda value: value.id)
    # def copy(self, default=None):
    #     default = dict(default or {}, name=_('%s (Copy)') % self.name)
    #     return super(Permissions, self).copy(default=default)
    @api.model
    def _default_director(self):
        return self.env['principal.table'].search([('id', '=', 1)], limit=1)

    @api.multi
    @api.depends('form_date')
    def _compute_get_date(self):
        for r in self:
            if r.form_date:
                locale.setlocale(locale.LC_TIME, "es_EC.utf-8")
                day_week = datetime.today().strftime("%A").title()
                day = datetime.today().strftime("%d")
                month = datetime.today().strftime("%B").title()
                year = datetime.today().strftime("%Y")
                r.hidden_date = _(u'%s %s de %s del %s') % (day_week, day, month, year)

    @api.one
    @api.depends('start_date', 'end_date')
    def _compute_duration(self):
        if self.start_date and self.end_date:
            start_date = fields.Date.from_string(self.start_date)
            end_date = fields.Date.from_string(self.end_date)
            rdelta = relativedelta(end_date, start_date)
            self.duration = _('%s Years %s Months %s Days') % (rdelta.years, rdelta.months, rdelta.days + 1)

    @api.multi
    def draft(self):
        self.state = 'draft'

    @api.multi
    def accepted(self):
        for record in self:
            sequential = self.env['ir.sequence'].next_by_code('permission.code')
            record.sequential_id = sequential
            record.state = 'accepted'

    @api.multi
    def done(self):
        self.state = 'done'

    @api.model
    @api.returns('self', lambda value: value.id)
    def create(self, values):
        res = self.search([('applicant_id', '=', values['applicant_id']),
                           ('state', '=', 'accepted')])
        if res:
            raise UserError('EL ALUMNO ACTUAL SE ENCUENTRA CON UN PERMISO EXISTENTE!')
        return super(Permissions, self).create(values)

    # @api.multi
    # def create(self):
    #     obj = self.env['courses.training.permission'].search('applicant_id')
    #     for record in obj:
    #         if record.state == 'accepted':
    #                 # if record.applicant_id:
    #                 # existing = self.search([('applicant_id','in',obj.ids),('id', 'not in', self.ids),('state','=', 'accepted')])
    #                 # if record.existing:
    #                 raise UserError('EL ALUMNO ACTUAL SE ENCUENTRA CON UN PERMISO EXISTENTE!')
    #     # return super(Permissions, self).create(values)
