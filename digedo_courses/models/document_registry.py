# -*- coding: utf-8 -*-
import logging
from odoo import api, fields, models, _

_logger = logging.getLogger(__name__)

DRAFT = [
    ('draft', 'Borrador'),
    ('accepted', 'Aceptado'),
    ('rejected', 'Rechazado')
]


class DocumentRegistry(models.Model):
    _name = 'courses.training.document.registry'
    _description = 'Document registry'
    _rec_name = 'paper_id'

    paper_id = fields.Char('OFICIO NO.', readonly=True)
    date = fields.Date(string='FECHA', required=True, default=fields.Date.context_today,
                       states={'draft': [('readonly', False)]}, readonly=True)
    applicant_id = fields.Many2one('res.partner', string='APLICANTE', ondelete='restrict', required=True,
                                   states={'draft': [('readonly', False)]}, readonly=True)
    division_id = fields.Many2one('courses.person.division', string='DIVISION', required=True,
                                  states={'draft': [('readonly', False)]}, readonly=True)
    approved_date = fields.Date(string='FECHA DE APROBACIÓN', required=True, default=fields.Date.context_today,
                                states={'draft': [('readonly', False)]}, readonly=True)

    school_ids = fields.One2many('school.degree', inverse_name="documentregistry_id", string="ESCUELA",
                                 states={'draft': [('readonly', False)]}, readonly=True)
    # school_id = fields.Many2one('courses.school', string='School', ondelete='restrict', required=True)
    # academic_degree_id = fields.Many2one('courses.school.academic.degree', string='Academic degree',
    #                                     ondelete='restrict', required=True)
    rank_id = fields.Many2one('courses.person.rank', string='RANGO', required=True,
                              states={'draft': [('readonly', False)]}, readonly=True)
    annex_number = fields.Char(string='ANEXO', size=128, required=True)
    state = fields.Selection(DRAFT, 'ESTADO', default='draft')

    @api.one
    @api.returns('self', lambda value: value.id)
    def copy(self, default=None):
        default = dict(default or {}, name=_('%s (Copy)') % self.name)
        return super(DocumentRegistry, self).copy(default=default)

    @api.multi
    def draft(self):
        self.state = 'draft'

    @api.multi
    def accepted(self):
        for record in self:
            sequential = self.env['ir.sequence'].next_by_code('valuation.code')
            record.paper_id = sequential
            record.state = 'accepted'

    @api.multi
    def rejected(self):
        self.state = 'rejected'
