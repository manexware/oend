from odoo import models, fields


class SchoolDegree(models.Model):
    _name = 'school.degree'
    _description = 'School and degree for a student'

    # documentregistry_id = fields.Many2one('courses.training.document.registry')
    school_id = fields.Many2one('courses.school', string='School', required=True, ondelete='restrict')
    academic_degree_id = fields.Many2one('career.school', string='Academic degree',  required=True, ondelete='cascade')
    certificate_name = fields.Char(string='Certificate', ondelete='restrict',  required=True)
    annex_file = fields.Binary("File", attachment=True)

    _order = 'school_id'
