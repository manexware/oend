from odoo import models, fields


class PermissionFile(models.Model):
    _name = 'permission.file'
    _description = 'attached files for student permissions'

    permission_id = fields.Many2one('courses.training.permission')
    file_name = fields.Char(string='Name', ondelete='restrict',  required=True)
    annex_file = fields.Binary("File", attachment=True)
    annex_file_name = fields.Char()

    _order = 'file_name'
