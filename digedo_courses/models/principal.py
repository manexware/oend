# -*- coding: utf-8 -*-
from odoo import api, fields, models


class Principal(models.Model):
    _name = 'principal.table'
    _description = 'Firma de directivas'

    name = fields.Many2one('res.partner', string=u'DIRECTOR', required=True, store=True)
    grade = fields.Many2one(string=u'RANGO', store=True, related='name.rank_id')
    signature = fields.Char(u'SUMILLA', required=True, store=True)
    institution = fields.Char(u'DIRECCIÓN / ESCUELA', required=True, store=True)

    @api.model
    def create(self, values):
        #a = values.get('name')
        b = values.get('signature')
        c = values.get('institution')
        values.update({'signature': b,'institution': c})
        values = self.values_upper(values)
        return super(Principal, self).create(values)

    @api.multi
    def write(self, values):
        #a = values.get('name')
        #if a:
        #    values.update({'name': a})
        b = values.get('signature')
        if b:
            values.update({'signature': b})
        c = values.get('institution')
        if c:
            values.update({'institution': b})
        values = self.values_upper(values)
        return super(Principal, self).write(values)

    def values_upper(self, values):
        #if 'name' in values:
        #    if values['name']:
        #        values['name'] = values['name'].upper()
        if 'signature' in values:
            if values['signature']:
                values['signature'] = values['signature'].upper()
        if 'institution' in values:
            if values['institution']:
                values['institution'] = values['signature'].upper()
        return values


