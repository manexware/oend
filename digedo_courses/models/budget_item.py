# -*- coding: utf-8 -*-

from odoo import _, models, fields, api


class BudgetItem(models.Model):
    _name = 'budget.item'

    name = fields.Char(string=u'ITEM PRESUPUESTARIO', size=128, required=True)

    @api.model
    def create(self, values):
        a = values.get('name')
        if a:
            values.update({'name': a.upper()})
        return super(BudgetItem, self).create(values)

    @api.multi
    def write(self, values):
        a = values.get('name')
        if a:
            values.update({'name': a.upper()})
        return super(BudgetItem, self).write(values)

