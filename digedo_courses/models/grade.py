from odoo import _, models, fields, api


class SieGrade(models.Model):
    _name = 'school.grade'
    _description = 'Grade'

    acronym = fields.Char(string=_('Acronym'), size=10, required=True, search='_search_acronym')
    name = fields.Char(string=_('Name'), size=96, required=True, search='_search_name')
    is_official = fields.Boolean(string=_('Is Official?'), help=_('Indicates if the marine is official'), default=False)
    order_ = fields.Integer(string=_('Order'), default=0)

    _sql_constraints = [
        ('acronym_uk', 'unique(acronym)', _('Acronym must be unique')),
        ('name_uk', 'unique(name)', _('Name must be unique')),
    ]

    def _search_name(self, operator, value):
        if operator == 'like':
            operator = 'ilike'
        if self.name:
            return [('name', operator, value)]

    def _search_acronym(self, operator, value):
        if operator == 'like':
            operator = 'ilike'
        if self.acronym:
            if len(self.acronym) == 4:
                return [('acronym', operator, value)]

    @api.one
    def copy(self, default=None):
        default = dict(default or {})
        copied_count = self.search_count(
            [('name', '=like', u"Copy of {}%".format(self.name))])
        if not copied_count:
            new_name = u"Copy of {}".format(self.name)
            new_acronym = u"Copy of {}".format(self.acronym)
        else:
            new_name = u"Copy of {} ({})".format(self.name, copied_count)
            new_acronym = u"Copy of {} ({})".format(self.acronym, copied_count)
        default['name'] = new_name
        default['acronym'] = new_acronym
        return super(SieGrade, self).copy(default)

    @api.multi
    @api.depends('display_name')
    def name_get(self):
        result = []
        for record in self:
            result.append((record.id, '%s' % record.acronym))
        return result
