# -*- coding: utf-8 -*-

from odoo import _, models, fields, api


class Career(models.Model):
    _name = 'career.school'
    _description = 'Career'

    school_id = fields.Many2one('courses.school', string=u'ESCUELA', ondelete='restrict', required=True)
    name = fields.Char(string=u'NOMBRE DE LA CARRERA', size=128, required=True)

    @api.model
    def create(self, values):
        a = values.get('name')
        if a:
            values.update({'name': a.upper()})
        return super(Career, self).create(values)

    @api.multi
    def write(self, values):
        a = values.get('name')
        if a:
            values.update({'name': a.upper()})
        return super(Career, self).write(values)

