from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class SiePromotion(models.Model):
    _name = 'sie.promotion'

    name = fields.Char(string='Name', required=True)
    display_name = fields.Char(string='Display Name', compute='_compute_display_name')

    @api.one
    @api.depends('name')
    def _compute_display_name(self):
        if self.name:
            prefix = _("Promocion")
            self.display_name = '%s %s' % (prefix, self.name)

    @api.onchange('name')
    def _check_digit(self):
        if self.name:
            unicodestring = self.name
            s = str(unicodestring).encode("utf-8")
            try:
                float(s)
            except ValueError:
                raise ValidationError(_(u'Not a number'))

    @api.multi
    @api.depends('display_name')
    def name_get(self):
        result = []
        for record in self:
            result.append((record.id, '%s' % record.display_name))
        return result
