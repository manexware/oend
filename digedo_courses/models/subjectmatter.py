# -*- coding: utf-8 -*-
from odoo.osv import expression
from odoo import api, fields, models, _


class SubjectMatter(models.Model):
    _name = 'courses.school.subject'
    _description = 'Subject-Matter'

    name = fields.Char(string='NOMBRE', size=128, required=True)
    code = fields.Char(string=u'CÓDIGO', size=10, required=True)
    knowledge_area_id = fields.Many2one('courses.school.knowledge.area', string='AREA DE CONOCIMIENTO',
                                        ondelete='restrict', required=True)
    # requirements = fields.Text(string='Requirements')
    # specific_objectives = fields.Text(string='Specific objectives')
    # conceptual_approach = fields.Text(string='Conceptual approach')

    _sql_constraints = [
        ('subject_matter_unique', 'unique(code, name)', _('The subject-matter must be unique!')),
    ]

    @api.model
    def create(self, values):
        a = values.get('name')
        b = values.get('code')
        values.update({'name': a.upper(), 'code': b.upper()})
        return super(SubjectMatter, self).create(values)

    @api.multi
    def write(self, values):
        a = values.get('name')
        if a:
            values.update({'name': a.upper()})
        b = values.get('code')
        if b:
            values.update({'code': b.upper()})
        return super(SubjectMatter, self).write(values)

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=80):
        args = args or []
        domain = []
        if name:
            domain = ['|', ('name', operator, name), ('code', operator, name)]
            if operator in expression.NEGATIVE_TERM_OPERATORS:
                domain = ['&', '!'] + domain[1:]
        ids = self.search(domain + args, limit=limit)
        return ids.name_get()

    @api.one
    @api.returns('self', lambda value: value.id)
    def copy(self, default=None):
        default = dict(default or {}, name=_('%s (Copy)') % self.name)
        return super(SubjectMatter, self).copy(default=default)
