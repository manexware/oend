# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class CurriculumSubject(models.Model):
    _name = 'curriculum.subject'
    _description = 'Materias de la Malla'

    subject_id = fields.Many2one('course.curriculum')
    subject_name = fields.Many2one('courses.school.subject', string='ASIGNATURA', size=128, required=True)
    cycle = fields.Char(string='CICLO', size=128, required=True)
