# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class Division(models.Model):
    _name = 'courses.person.division'
    _description = 'Division'

    name = fields.Char(string='NOMBRE', size=128, required=True)
    acronym = fields.Char(string='ACRONIMO', size=8, required=True)

    _sql_constraints = [
        ('acronym_unique', 'unique(acronym)', _('The acronym must be unique!')),
    ]

    @api.model
    def create(self, values):
        a = values.get('acronym')
        b = values.get('name')
        values.update({'acronym': a.upper(), 'name': b.upper()})
        return super(Division, self).create(values)

    @api.multi
    def write(self, values):
        a = values.get('acronym')
        if a:
            values.update({'acronym': a.upper()})
        b = values.get('name')
        if b:
            values.update({'name': b.upper()})
        return super(Division, self).write(values)
