from odoo import models, fields, api

ACADEMIC = [
    ('approved','APROBADO'),
    ('not','NO APROBADO'),
    ('withdrawn','RETIRADO')
]


class StudentCourse(models.Model):
    _name = 'subject.curriculum'
    _description = 'Malla curricular'
    _rec_name = 'subject_id'

    curriculum_id = fields.Many2one('courses.training.permission')
    subject_code = fields.Char(store=True)
    subject_id = fields.Char(store=True)
    score = fields.Float(store=True)
    academic_status = fields.Selection(ACADEMIC, store=True)
    reference_id = fields.Char(store=True)

    @api.model
    def create(self, values):
        a = values.get('subject_code')
        b = values.get('subject_id')
        c = values.get('reference_id')
        values.update({'subject_code': a, 'subject_id': b, 'reference_id': c})
        values = self.values_upper(values)
        return super(StudentCourse, self).create(values)

    @api.multi
    def write(self, values):
        a = values.get('subject_code')
        if a:
            values.update({'subject_code': a})
        b = values.get('subject_id')
        if b:
            values.update({'subject_id': b})
        c = values.get('reference_id')
        if c:
            values.update({'reference_id': c})
        values = self.values_upper(values)
        return super(StudentCourse, self).write(values)

    def values_upper(self, values):
        if 'reference_id' in values:
            if values['reference_id']:
                values['reference_id'] = values['reference_id'].upper()
        if 'subject_id' in values:
            if values['subject_id']:
                values['subject_id'] = values['subject_id'].upper()
        if 'subject_code' in values:
            if values['subject_code']:
                values['subject_code'] = values['subject_code'].upper()
        return values



