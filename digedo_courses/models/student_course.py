from odoo import models, fields, api
OPTIONS = [
    ('s','SI'),
    ('n','NO')
]
ACADEMIC = [
    ('in_course','EN CURSO'),
    ('approved','APROBADO'),
    ('not','NO APROBADO'),
    ('withdrawn','RETIRADO')
]


class StudentCourse(models.Model):
    _name = 'student.course'
    _description = 'ALUMNOS'
    _rec_name = 'student_id'

    school_training_id = fields.Many2one('school.training', ondelete='cascade', string=u"CURSOS")
    student_id = fields.Many2one('res.partner', string=u'ALUMNO')
    grade_id = fields.Many2one('courses.person.rank', related='student_id.rank_id', string=u'GRADO', readonly=True)
    specialty_id = fields.Many2one('courses.person.specialty', related='student_id.specialty_id', string='ESPECIALIDAD', readonly=True)
    contract = fields.Binary(attachment=True, string="CONTRATO")
    contract_name = fields.Char()
    academic_status = fields.Selection(ACADEMIC, store=True, default='in_course')
    is_finished = fields.Selection(OPTIONS, store=True, default='n', required=True)
    finished = fields.Char(compute='_replace_finished', store=True)
    is_earning_year_finished = fields.Char(compute="_compute_earning_check", readonly=True, store=True)
    file = fields.Binary(attachment=True, string="ARCHIVO ADJUNTO")
    file_name = fields.Char()

    @api.one
    @api.depends('school_training_id.earning_year','school_training_id.modality_id')
    def _compute_earning_check(self):
        if self.school_training_id.modality_id.id == 1:
            if self.school_training_id.earning_year:
                if fields.Date.from_string(fields.Date.today()) >= fields.Date.from_string(self.school_training_id.earning_year):
                    self.is_earning_year_finished = 'SI'
                else:
                    self.is_earning_year_finished = 'NO'
        else:
            self.is_earning_year_finished = 'NO APLICA'

    @api.one
    @api.depends('is_finished')
    def _replace_finished(self):
        if self.is_finished == 's':
            self.finished = "SI"
        else:
            self.finished = "NO"
