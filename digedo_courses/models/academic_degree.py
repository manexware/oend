# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class AcademicDegree(models.Model):
    _name = 'courses.school.academic.degree'
    _description = 'Academic Degree'

    # acronym = fields.Char(string='ACRONIMO', size=128, required=True)
    name = fields.Char(string=u'NOMBRE', size=128, required=True)

    @api.model
    def create(self, values):
        # a = values.get('acronym')
        b = values.get('name')
        values.update({'name': b.upper()})
        return super(AcademicDegree, self).create(values)

    @api.multi
    def write(self, values):
        #a = values.get('acronym')
        #if a:
        #    values.update({'acronym': a.upper()})
        b = values.get('name')
        if b:
            values.update({'name': b.upper()})
        return super(AcademicDegree, self).write(values)

    @api.one
    @api.returns('self', lambda value: value.id)
    def copy(self, default=None):
        default = dict(default or {}, name=_('%s (Copy)') % self.name)
        return super(AcademicDegree, self).copy(default=default)
