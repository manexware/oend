# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class CourseLevel(models.Model):
    _name = 'courses.level'

    name = fields.Char(string=u'NIVEL DE CURSO', size=128, required=True)
    acronym = fields.Char(string=u'ACRÓNIMO', size=8, required=True)

    _sql_constraints = [
        ('acronym_unique', 'unique(acronym)', _('The acronym must be unique!')),
    ]

    @api.model
    def create(self, values):
        a = values.get('acronym')
        b = values.get('name')
        values.update({'acronym': a.upper(), 'name': b.upper()})
        return super(CourseLevel, self).create(values)

    @api.multi
    def write(self, values):
        a = values.get('acronym')
        if a:
            values.update({'acronym': a.upper()})
        b = values.get('name')
        if b:
            values.update({'name': b.upper()})
        return super(CourseLevel, self).write(values)
