# -*- coding: utf-8 -*-
from odoo import fields, models


class CourseCurriculum(models.Model):
    _name = 'course.curriculum'
    _description = 'Malla Curricular'

    school_id = fields.Many2one('courses.school', string='ESCUELA', ondelete='restrict', required=True)
    career_id = fields.Many2one('career.school', string='CARRERA', ondelete='restrict', required=True)
    start_date = fields.Date(default=fields.date.today(), required=True)
    end_date = fields.Date(default=fields.date.today(), required=True)
    curriculum_subject_ids = fields.One2many('curriculum.subject', inverse_name='subject_id',
                                             string='ASIGNATURAS')

