# -*- coding: utf-8 -*-
from odoo.exceptions import ValidationError
from odoo import api, fields, models, _


class KnowledgeArea(models.Model):
    _name = 'courses.school.knowledge.area'
    _description = 'Open Field'

    name = fields.Char(size=128, required=True)
    child_ids = fields.One2many('courses.school.field.area', inverse_name="parent_id")

    # @api.constrains('parent_id')
    # def _check_parent_id(self):
    #    if not self._check_recursion():
    #        raise ValidationError(_('Error! You cannot create recursive knowledge areas.'))

    # @api.multi
    # def name_get(self):
    #    res = []
    #    for record in self:
    #        name = record.name
    #        if record.parent_id:
    #            name = '%s / %s' % (record.parent_id.name_get()[0][1], name)
    #        res.append((record.id, name))
    #    return res

    @api.model
    def create(self, values):
        a = values.get('name')
        values.update({'name': a.upper()})
        return super(KnowledgeArea, self).create(values)

    @api.multi
    def write(self, values):
        a = values.get('name')
        if a:
            values.update({'name': a.upper()})
        return super(KnowledgeArea, self).write(values)


class FieldArea(models.Model):
    _name = 'courses.school.field.area'
    _description = 'Field Area'

    name = fields.Char(size=128, required=True)
    parent_id = fields.Many2one('courses.school.knowledge.area', ondelete='cascade', required=True)
    child_ids = fields.One2many('courses.school.detailed.area', inverse_name="area_id")

    @api.model
    def create(self, values):
        a = values.get('name')
        values.update({'name': a.upper()})
        return super(FieldArea, self).create(values)

    @api.multi
    def write(self, values):
        a = values.get('name')
        if a:
            values.update({'name': a.upper()})
        return super(FieldArea, self).write(values)


class DetailedArea(models.Model):
        _name = 'courses.school.detailed.area'
        _description = 'Field Area'

        name = fields.Char(size=128, required=True)
        area_id = fields.Many2one('courses.school.field.area', ondelete='cascade', required=True)

        @api.model
        def create(self, values):
            a = values.get('name')
            values.update({'name': a.upper()})
            return super(DetailedArea, self).create(values)

        @api.multi
        def write(self, values):
            a = values.get('name')
            if a:
                values.update({'name': a.upper()})
            return super(DetailedArea, self).write(values)