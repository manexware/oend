# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
import logging

_logger = logging.getLogger(__name__)


class Rank(models.Model):
    _name = 'courses.person.rank'
    _description = 'RANGO'

    name = fields.Char(string='NOMBRE', size=128, required=True)
    acronym = fields.Char(string='ACRONIMO', size=6, required=True)

    _sql_constraints = [
        ('acronym_unique', 'unique(acronym)', _('The acronym must be unique!')),
    ]

    @api.model
    def create(self, values):
        a = values.get('acronym')
        b = values.get('name')
        values.update({'acronym': a.upper(), 'name': b.upper()})
        return super(Rank, self).create(values)

    @api.multi
    def write(self, values):
        a = values.get('acronym')
        if a:
            values.update({'acronym': a.upper()})
        b = values.get('name')
        if b:
            values.update({'name': b.upper()})
        return super(Rank, self).write(values)
