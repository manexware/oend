# -*- coding: utf-8 -*-
from odoo.osv import expression
from odoo import api, fields, models

FORMACION = [
    ('arm','ARM'),
    ('esp','ESP'),
    ('srv','SRV'),
    ('tnc','TNC')
]

TIPO = [
    ('military', 'MILITAR'),
    ('civil', 'CIVIL')
    ]


class People(models.Model):
    _name = 'res.partner'
    _inherit = 'res.partner'

    # fullname = fields.Char(string='Full Name', size=123, required=False)
    name = fields.Char(string='Names', size=123)
    # last_name = fields.Char(string='Last name', size=123)
    citizen_id = fields.Char(string='C.I.', size=10)
    # military information
    division_id = fields.Many2one('courses.person.division', ondelete='restrict')
    rank_id = fields.Many2one('courses.person.rank', string='GRADO', ondelete='restrict')
    specialty_id = fields.Many2one('courses.person.specialty', string='ESPECIALIDAD', ondelete='restrict')
    active_in_force = fields.Boolean(string='Currently is active in the force?', default=True)
    admission_date = fields.Date(string='Admission date')
    promotion_date = fields.Date(string='Promotion date')
    formation = fields.Selection(FORMACION, string=u"FORMACIÓN")
    formation_id = fields.Char(compute='_replace_formation')
    responsability = fields.Char()
    city_id = fields.Char()
    phone = fields.Char()
    cellphone = fields.Char()
    address = fields.Char()
    email = fields.Char()
    kind = fields.Selection(TIPO, required=True, default='military')
    kind_id = fields.Char(compute='_replace_kind')

    _sql_constraints = [
        ('email_unique', 'UNIQUE(email)', u'EMAIL DEBE SER ÚNICO'),
        ('citizen_unique', 'UNIQUE(citizen_id)', u'USUARIO DEBE SER ÚNICO'),
    ]

    @api.one
    @api.depends('kind')
    def _replace_kind(self):
         if self.kind == 'military':
             self.kind_id = "MILITAR"
         else:
             self.kind_id = "CIVIL"

    @api.one
    @api.depends('formation')
    def _replace_formation(self):
        if self.formation == 'arm':
            self.formation_id = "ARM"
        elif self.formation == 'esp':
            self.formation_id = "ESP"
        elif self.formation == 'srv':
            self.formation_id = "SRV"
        else:
            self.formation_id = "TNC"

    @api.model
    def create(self, values):
        a = values.get('name')
        b = values.get('address')
        values.update({'name': a, 'address': b})
        n = values.get('suffix')
        if n:
            values.update({'suffix': n()})
        values = self.values_upper(values)
        return super(People, self).create(values)

    @api.multi
    def write(self, values):
        a = values.get('name')
        if a:
            values.update({'name': a})
        b = values.get('address')
        if b:
            values.update({'address': b})
        n = values.get('suffix')
        if n:
            values.update({'suffix': n})
        values = self.values_upper(values)
        return super(People, self).write(values)

    def values_upper(self, values):
        if 'name' in values:
            if values['name']:
                values['name'] = values['name'].upper()
        if 'address' in values:
            if values['address']:
                values['address'] = values['address'].upper()
        return values

    # @api.model
    # def name_search(self, name, args=None, operator='ilike', limit=80):
    #     args = args or []
    #     domain = []
    #     if name:
    #         domain = ['|', ('name', operator, name)]
    #         if operator in expression.NEGATIVE_TERM_OPERATORS:
    #             domain = ['&', '!'] + domain[1:]
    #     ids = self.search(domain + args, limit=limit)
    #     return ids.name_get()

    @api.multi
    def toggle_active_in_force(self):
        for record in self:
            record.active_in_force = not record.active_in_force

    @api.multi
    def check_data(self):
        db_model = self.env['base.external.dbsource'].search([('name','=','Personal')],limit=1)
        sql_query = 'select * from consultadigedo.v_personal_mil_dir where cedula =' + "'" + self.citizen_id + "'"
        sql_query_civ = 'select * from consultadigedo.v_personal_civ where cedula =' + "'" + self.citizen_id + "'"
        res = db_model.execute(sql_query)
        res2 = db_model.execute(sql_query_civ)
        values = {}
        if res:
            nombre = res[0]._row[0]
            grado_id = self.env['courses.person.rank'].search([('acronym','=',res[0]._row[3])],limit=1).id
            direccion = res[0]._row[17]
            telefono = res[0]._row[18]
            celular = res[0]._row[19]
            fecha_ingreso = res[0]._row[14]
            reparto = self.env['courses.person.division'].search([('acronym','=',res[0]._row[6])],limit=1).id
            especializacion = self.env['courses.person.specialty'].search([('acronym','=',res[0]._row[5])],limit=1).id
            cargo = res[0]._row[7]
            ciudad = res[0]._row[16]
            values['name'] = nombre
            if grado_id:
                values['rank_id'] = grado_id
            values['address'] = direccion
            values['phone'] = telefono
            values['admission_date'] = fecha_ingreso
            if reparto:
                values['division_id'] = reparto
            values['cellphone'] = celular
            if especializacion:
                values['specialty_id'] = especializacion
            values['responsability'] = cargo
            if ciudad:
                values['city_id'] = ciudad
            values['kind'] = 'military'
            self.write(values)
        elif res2:
            nombre = res2[0]._row[0]
            grado_id = self.env['courses.person.rank'].search([('acronym', '=', 'SERPUB')], limit=1).id
            direccion = res2[0]._row[16]
            telefono = res2[0]._row[17]
            celular = res2[0]._row[18]
            # fecha_ingreso = res2[0]._row[14]
            reparto = self.env['courses.person.division'].search([('acronym', '=', res2[0]._row[5])], limit=1).id
            # especializacion = self.env['courses.person.specialty'].search([('acronym', '=', res[0]._row[5])],limit=1).id
            cargo = res2[0]._row[6]
            ciudad = res2[0]._row[15]
            # formacion = res2[0]._row[4]
            # ciudad = self.env['res.state.city'].search([('name','=',res2[0]._row[16])],limit=1).id
            # nacionalidad = self.env['res.country'].search([('name','=',res2[0]._row[13])],limit=1).id
            values['name'] = nombre
            values['rank_id'] = grado_id
            values['address'] = direccion
            values['phone'] = telefono
            # self.admission_date = fecha_ingreso
            if reparto:
                values['division_id'] = reparto
            values['cellphone'] = celular
            # if especializacion:
              #  self.specialty_id = especializacion
            values['responsability'] = cargo
            if ciudad:
                values['city_id']= ciudad
            values['kind'] = 'civil'
            self.write(values)
        return True

    @api.multi
    @api.depends('rank_id', 'name')
    def name_get(self):
        res = []
        for p in self:
            if p.kind == 'military':
                name = '%s %s' % (p.rank_id.acronym, p.name)
                res.append((p.id, name))
            else:
                name = '%s' % p.name
                res.append((p.id, name))
        return res


