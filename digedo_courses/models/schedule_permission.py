from odoo import models, fields, api

DAYS = [
    ('lunes', 'LUNES'),
    ('martes', 'MARTES'),
    ('miercoles', 'MIERCOLES'),
    ('jueves', 'JUEVES'),
    ('viernes', 'VIERNES'),
    ('sabado', 'SABADO'),
    ('domingo', 'DOMINGO'),
]


class SchedulePermission(models.Model):
    _name = 'schedule.permission'
    _description = 'Time and schedule for courses'

    school_permission_id = fields.Many2one('courses.training.permission')
    subject_id = fields.Many2one('subject.curriculum', domain="[('curriculum_id','=',school_permission_id)]")
    schedule_days_id = fields.Selection(DAYS, ondelete='cascade')
    schedule_start_hour = fields.Char(store=True, required=True)
    schedule_end_hour = fields.Char(store=True, required=True)

    @api.one
    @api.depends('course_training_permission')
    def _get_subject(self):
        if self.course_training_permission.subject_curriculum_ids.subject_id:
            return {'value': {'subject_id': self.course_training_permission.subject_curriculum_ids.subject_id}}
        return {'value': {}}




