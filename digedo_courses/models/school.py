# -*- coding: utf-8 -*-
from odoo.osv import expression
from odoo import _, models, fields, api


class School(models.Model):
    _name = 'courses.school'
    _description = 'School'

    abbreviation = fields.Char(string='Abbreviation', size=32)
    name = fields.Char(string='Name', size=128, required=True)
    country_id = fields.Many2one('res.country', string='Country', ondelete='restrict', required=True)

    _sql_constraints = [
        ('name_unq', 'unique(name, country_id)', _('School must be unique per country'))
    ]

    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=50):
        args = args or []
        domain = []
        if name:
            domain = ['|', ('abbreviation', operator, name), ('name', operator, name)]
            if operator in expression.NEGATIVE_TERM_OPERATORS:
                domain = ['&', '!'] + domain[1:]
        schools = self.search(domain + args, limit=limit)
        return schools.name_get()

    # @api.multi
    # @api.depends('name', 'country_id')
    # def name_get(self):
    #     res = []
    #     for record in self:
    #         name = '%s (%s)' % (record.name, record.country_id.name)
    #         res.append((record.id, name))
    #     return res

    @api.model
    def create(self, values):
        n = values.get('name')
        values.update({'name': n.upper()})
        a = values.get('abbreviation')
        if a:
            values.update({'abbreviation': a.upper()})
        return super(School, self).create(values)

    @api.multi
    def write(self, values):
        n = values.get('name')
        if n:
            values.update({'name': n.upper()})
        a = values.get('abbreviation')
        if a:
            values.update({'abbreviation': a.upper()})
        return super(School, self).write(values)
