from odoo import models, fields, api


class Sector(models.Model):
    _name = 'school.training.sector'

    name = fields.Char(string='Name', required=True)
    code = fields.Char('Code', size=4, required=True)

    _sql_constraints = [
        ('name_uk', 'unique(name)', _('Name must be unique'))
    ]

