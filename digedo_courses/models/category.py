from odoo import _, models, fields


class SieCategory(models.Model):
    _name = 'school.training.category'

    name = fields.Char(string='NOMBRE', required=True)
    code = fields.Char(string='CODIGO', size=4, required=True)

    _sql_constraints = [
        ('name_uk', 'unique(name)', _('Name must be unique'))
    ]
