# -*- coding: utf-8 -*-
import logging
import locale
from datetime import datetime
from odoo import api, fields, models, _
_logger = logging.getLogger(__name__)

DRAFT = [
    ('draft', 'BORRADOR'),
    ('accepted', 'ACEPTADO'),
    ('rejected', 'RECHAZADO')
]


class ValuationOfCertificate(models.Model):
    _name = 'courses.training.certificate.valuation'
    _description = 'Valuation of Certificate'

    sequential_id = fields.Char(store=True, string=u'No. DE OFICIO', size=80, readonly=True)
    # oficio = fields.Char(store=True, string='No. DE OFICIO', size=80, readonly=True)
    name = fields.Many2one('course.name', size=96, required=True,
                           readonly=True, states={'not executed': [('readonly', False)]})
    date = fields.Date(string=u'FECHA', required=True, default=fields.Date.context_today)
    hidden_date = fields.Char(compute='_compute_get_date', store=True)
    type_id = fields.Many2one('courses.training.type', string=u'TIPO DE CURSO', ondelete='restrict', required=True)
    valuation_id = fields.Many2one('courses.training.valuation', string=u'VALORACIÓN', ondelete='restrict', required=True,
                                   domain="[('type_id', '=', type_id)]")
    category_id = fields.Many2one('courses.level', string=u'NIVEL DE ESTUDIOS', ondelete='restrict')
    applicant_id = fields.Many2one('res.partner', string=u'APLICANTE', ondelete='restrict', required=True)
    school_id = fields.Many2one('courses.school', string=u'INSTITUCION ACADEMICA', ondelete='restrict', required=True)
    academic_degree_id = fields.Many2one('courses.school.academic.degree', ondelete='restrict')
    approved_date = fields.Date(string=u'FECHA DE APROBACIÓN', required=True, default=fields.Date.context_today)
    score = fields.Float(digits=(1, 3), related='valuation_id.score', store=True)
    # total_score = fields.Float(string='PUNTAJE TOTAL', digits=(2, 2), compute='_compute_total_score')
    state = fields.Selection(DRAFT, string=u'ESTADO', default='draft')
    reference = fields.Char(string=u'REFERENCIA',
                            readonly=True, states={'not executed': [('readonly', False)]})

    sender = fields.Char(u'DESTINATARIO', store=True)
    # DIRECTIVA
    director_id = fields.Many2one('principal.table', string=u'DIRECTIVA', store=True)
    director = fields.Many2one(related="director_id.name", readonly=True, store=True)
    grade = fields.Many2one(related="director_id.grade", readonly=True, store=True)
    signature = fields.Char(u'SUMILLA', related="director_id.signature", readonly=True, store=True)
    institution = fields.Char(u'ESCUELA / DIRECCIÓN', related="director_id.institution", readonly=True, store=True)

    # @api.model
    # def create(self, values):
    #     n = values.get('suffix')
    #     if n:
    #         values.update({'suffix': n()})
    #     values = self.values_upper(values)
    #     return super(ValuationOfCertificate, self).create(values)
    #
    # @api.multi
    # def write(self, values):
    #     n = values.get('suffix')
    #     if n:
    #         values.update({'suffix': n})
    #     values = self.values_upper(values)
    #     return super(ValuationOfCertificate, self).write(values)
    #
    # def values_upper(self, values):
    #     if 'suffix' in values:
    #         values['suffix'] = values['suffix'].upper()
    #     return values

    # @api.multi
    # @api.depends('applicant_id')
    # def _compute_total_score(self):
    #     obj = self.env['courses.training.certificate.valuation']
    #     for record in self:
    #         x = 0
    #         if record.applicant_id:
    #             result = obj.search_read([('applicant_id', '=', record.applicant_id.id)], fields=['score'])
    #             x = sum(x['score'] for x in result)
    #         record.total_score = x
    @api.multi
    @api.depends('date')
    def _compute_get_date(self):
        for r in self:
            if r.date:
                locale.setlocale(locale.LC_TIME, "es_EC.utf-8")
                day_week = datetime.today().strftime("%A").title()
                day = datetime.today().strftime("%d")
                month = datetime.today().strftime("%B").title()
                year = datetime.today().strftime("%Y")
                r.hidden_date = _(u'%s %s de %s del %s') % (day_week, day, month, year)

    @api.model
    def create(self, values):
        values['usuario'] = self.env.user.partner_id.id
        return super(ValuationOfCertificate, self).create(values)

    @api.onchange('type_id')
    def onchange_type_id(self):
        if self.valuation_id:
            self.valuation_id = False

    @api.one
    @api.returns('self', lambda value: value.id)
    def copy(self, default=None):
        default = dict(default or {}, name=_('%s (Copy)') % self.name)
        return super(ValuationOfCertificate, self).copy(default=default)

    @api.multi
    def draft(self):
        self.state = 'draft'

    @api.multi
    def accepted(self):
        for record in self:
            sequential = self.env['ir.sequence'].next_by_code('valuation.code')
            record.sequential_id = sequential
            record.state = 'accepted'

    @api.multi
    def rejected(self):
        self.state = 'rejected'

    @api.model
    def create(self, values):
        b = values.get('reference')
        if b:
            values.update({'reference': b.upper()})
        return super(ValuationOfCertificate, self).create(values)

    @api.multi
    def write(self, values):
        b = values.get('reference')
        if b:
            values.update({'reference': b.upper()})
        return super(ValuationOfCertificate, self).write(values)

    def values_upper(self, values):
        if 'reference' in values:
            values['reference'] = values['reference'].upper()
            return values





