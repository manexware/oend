# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class CourseName(models.Model):
    _name = 'course.name'
    _description = 'Name of existing courses'

    name = fields.Char(string='NOMBRE DEL CURSO', size=128, required=True)

    _sql_constraints = [
        ('acronym_unique', 'unique(name)', _(u'EL NOMBRE DEBE SER ÚNICO!')),
    ]

    @api.model
    def create(self, values):
        b = values.get('name')
        values.update({'name': b.upper()})
        return super(CourseName, self).create(values)

    @api.multi
    def write(self, values):
        b = values.get('name')
        if b:
            values.update({'name': b.upper()})
        return super(CourseName, self).write(values)
