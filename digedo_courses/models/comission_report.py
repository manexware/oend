from odoo import models, fields, api,_


class ComissionReport(models.Model):
    _name = 'comission.report'

    report_id = fields.Many2one('school.training')
    concept_id= fields.Char()
    file = fields.Binary(string=u'ARCHIVO ADJUNTO', attachment=True)
    file_name = fields.Char()

    @api.model
    def create(self, values):
        b = values.get('concept_id')
        values.update({'concept_id': b.upper()})
        return super(ComissionReport, self).create(values)

    @api.multi
    def write(self, values):
        b = values.get('concept_id')
        if b:
            values.update({'concept_id': b.upper()})
        return super(ComissionReport, self).write(values)