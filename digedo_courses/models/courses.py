from odoo import _, models, fields, api, exceptions
from dateutil.relativedelta import relativedelta
from datetime import date

TRAINING_STATES = [
    ('not executed', 'NO INICIADO'),
    ('suspend', 'SUSPENDIDO'),
    ('running', 'EN EJECUCION'),
    ('executed', 'FINALIZADO')
]

PLACES = [
    ('abroad', 'EXTERIOR'),
    ('national', 'NACIONAL')
]


class SchoolTraining(models.Model):
    _name = 'school.training'
    _description = 'Training'

    @api.one
    @api.constrains('end_date', 'start_date')
    def _check_end_date(self):
        if self.start_date and self.end_date:
            start_date = fields.Datetime.from_string(self.start_date)
            end_date = fields.Datetime.from_string(self.end_date)
            if end_date < start_date:
                end_date = start_date + relativedelta(months=+ 3)
                self.end_date = end_date
                raise Warning("LA FECHA DE FIN DE CURSO TIENE QUE SER MAYOR A LA FECHA DE INICIO")
        else:
            raise Exception("SELECCIONE LAS FECHAS DE INICIO Y FIN DE CURSO")
    #
    # @api.one
    # @api.depends('res.country')
    # def _country_selection(self):
    #     place = self.env['res.country'].search([('name','=','Ecuador')])
    #     if place:
    #         self.place_id = 'national'

    @api.one
    @api.depends('start_date', 'end_date')
    def _compute_duration(self):
        if self.start_date and self.end_date:
            start_date = fields.Date.from_string(self.start_date)
            end_date = fields.Date.from_string(self.end_date)
            rdelta = relativedelta(end_date, start_date)
            self.duration = _('%s Years %s Months %s Days') % (rdelta.years, rdelta.months, rdelta.days + 1)

    @api.one
    @api.depends('end_date', 'place_id')
    def _compute_earning_year(self):
        if self.end_date:
            end_date = fields.Date.from_string(self.end_date)
            if self.place_id == 'abroad':
                delta = end_date + relativedelta(years=3)
                self.earning_year = delta
            elif self.place_id == 'national':
                delta = end_date + relativedelta(years=2)
                self.earning_year = delta

    @api.one
    @api.depends('student_ids')
    def _compute_count(self):
        if self.student_ids:
            self.student_count = len(self.student_ids)

    @api.one
    @api.depends('place_id')
    def _replace_place(self):
        if self.place_id == 'abroad':
            self.places = "EXTERIOR"
        else:
            self.places = "NACIONAL"

    name = fields.Many2one('course.name',string=u'NOMBRE DEL PROGRAMA', size=96, required=True,
                           readonly=True, states={'not executed': [('readonly', False)]})
    macroprocess_id = fields.Many2one('courses.macroprocess', string=u'MACROPROCESO', ondelete='restrict',
                                      readonly=True, states={'not executed': [('readonly', False)]})
    category_id = fields.Many2one('courses.level', ondelete='restrict',
                                  readonly=True, states={'not executed': [('readonly', False)]})
    country_id = fields.Many2one('res.country', ondelete='set null',
                                 readonly=True, states={'not executed': [('readonly', False)]})
    edu_inst_id = fields.Many2one('courses.school', ondelete='restrict', readonly=True,
                                  states={'not executed': [('readonly', False)]})
    modality_id = fields.Many2one('courses.modality',
                                  readonly=True, states={'not executed': [('readonly', False)]})
    start_date = fields.Date(help=_('(mm/dd/yyyy)'),default=fields.Date.context_today,
                             readonly=True, states={'not executed': [('readonly', False)]})
    end_date = fields.Date(help=_('(mm/dd/yyyy)'), default=fields.Date.context_today,
                           readonly=True, states={'not executed': [('readonly', False)]})
    duration = fields.Char(compute='_compute_duration', readonly=True)
    note = fields.Text(u'OBSERVACIONES')
    place_id = fields.Selection(PLACES, default='abroad')
    places = fields.Char(compute='_replace_place')
    state = fields.Selection(TRAINING_STATES, default='not executed')
    student_ids = fields.One2many('student.course', inverse_name="school_training_id", string=u'ALUMNOS',
                                  states={'not executed': [('readonly', False)]}, ondelete='cascade')
    student_count = fields.Integer(compute='_compute_count')
    financial_detail_ids = fields.One2many('school.training.financial.detail', inverse_name="training_id",
                                           string=u'DETALLE FINANCIERO',
                                           states={'not executed': [('readonly', False)]})
    earning_year = fields.Date(help=_('(mm/dd/yyyy)'), readonly=True,
                               states={'not executed': [('readonly', False)]})
    reference = fields.Char(string='REFERENCIA',
                            readonly=True, states={'not executed': [('readonly', False)]})
    report_ids = fields.One2many('comission.report', string=u'INFORME DE COMISION', inverse_name="report_id",
                                 states={'not executed': [('readonly', False)]})

    @api.model
    def create(self, values):
        b = values.get('reference')
        if b:
            values.update({'reference': b.upper()})
        return super(SchoolTraining, self).create(values)

    @api.multi
    def write(self, values):
        b = values.get('reference')
        if b:
            values.update({'reference': b.upper()})
        return super(SchoolTraining, self).write(values)

    def values_upper(self, values):
      if 'reference' in values:
         values['reference'] = values['reference'].upper()
         return values

    @api.one
    def suspend(self):
        self.state = 'suspend'

    @api.one
    def by_running(self):
        self.state = 'by running'

    @api.one
    def canceled(self):
        self.state = 'canceled'

    @api.one
    def running(self):
        self.state = 'running'

    @api.one
    def executed(self):
        self.state = 'executed'

    # @api.model
    # def search(self, args, offset=0, limit=None, order=None, count=False):
    #     if self._uid == 1:
    #         return super(SchoolTraining, self).search(args, offset=offset, limit=limit, order=order, count=count)
    #     self.env.cr.execute("""SELECT 1
    #         FROM res_groups_users_rel gu JOIN res_groups g ON g.id = gu.gid
    #         WHERE uid = %s AND name ~ 'Training Manager'""" % self._uid)
    #     flag = self.env.cr.fetchone()
    #     if not flag:
    #         self.env.cr.execute("""SELECT n.id
    #             FROM school_training_employee_rel t
    #             JOIN hr_employee e ON e.id = t.student_id
    #             JOIN resource_resource r ON r.id = e.resource_id
    #             JOIN school_training n ON n.id = t.training_id
    #             WHERE user_id = %s""" % self._uid)
    #         result = self.env.cr.fetchall()
    #         ids = set()
    #         for r in result:
    #             ids.add(r[0])
    #         args.append(('id', 'in', list(ids)))
    #     return super(SchoolTraining, self).search(args, offset, limit, order, count=False)
    #
